<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--bootsrap css -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!--jquery-->
    <script src="jquery/jquery-3.2.1.min.js"></script>
    <!--<script src="jquery/jquery.mycart.js"></script>-->
    <!--bootstrap js-->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!--css-->
    <link rel="stylesheet" href="css/home.css"> 
    <link rel="stylesheet" href="css/slideshow.css"> 
    <!--javascript-->
    <?php session_start(); ?>
    </head>
    <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
    <div class="navbar-header">
   <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>-->

      <a class="navbar-brand" href="index.php">SGR Online Food Ordering</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
    <ul class="nav navbar-nav">
    <li > <a title="Home" href="index.php">Home</a></li>
    <li > <a title="Breakfast" href="index.php">Breakfast</a></li>
    <li class="dropdown">
               <a class="dropdown-toggle" data-toggle="dropdown" href="#">Bevarages</a>
               <ul class="dropdown-menu">
               <li><a href="#">Non Alcoholic</a></li>
               <li><a href="#">Alcoholic</a></li>
               </ul>
    </li>                
    <li > <a title="Fruits" href="#">Fruits</a></li>
    </ul>
    <nav class="nav navbar-nav navbar-right">
    <a id="cartbtn" data-toggle="modal" data-target="#myCart"><span class="glyphicon glyphicon-shopping-cart my-cart-icon"/></a></nav>
    <?php
    session_start(); 
    if (isset($_SESSION['userId'])) {?>
    <nav class="nav navbar-nav navbar-right">
    <span id="fly" class="nav navbar-nav">Signed in as:<span>
      <button id="dropdown-button" class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
      <?php echo $_SESSION['userId'] ?>
    <span class="caret"></span></button>
    <div class="dropdown-menu" id="signout">
      <a title="sign out" href="signout.php">Sign Out</a>
    </div>

    </span></span> 
  </nav>
  <?php if (isset($_SESSION['seat'])): ?>
    <nav  style="margin-top: 8px;margin-right: 5px" class="nav navbar-nav navbar-right">
    <a class="btn btn-primary"href="currentOrder.php">View Order</a>
  </nav>
  <?php endif ?>
  <?php if ($_SESSION['userId']=='admin'): ?>
       <nav  style="margin-top: 8px;margin-right: 5px" class="nav navbar-nav navbar-right">
    <a class="btn btn-success" href="admin/index.php">Admin</a>
  </nav>

  <?php endif ?>
  <?php if ($_SESSION['userId']=='manager'): ?>
       <nav  style="margin-top: 8px;margin-right: 5px" class="nav navbar-nav navbar-right">
    <a class="btn btn-success" href="manager/index.php">Manager</a>
  </nav>

  <?php endif ?>

    <?php }else{ ?>
    <nav id="btnSignIn" class="nav navbar-nav navbar-right dropdown">
    <button id="dropdown-button" class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Sign In
    <span class="caret"></span></button>
    <!--handler after creating account-->
    <?php if (isset($_GET['createFlag']) && $_GET['createFlag'] == 1): ?>
      <script type="text/javascript">document.getElementById("dropdown-button").click();</script>
    <?php endif ?>


    <!--Log in part-->
    <?php include 'login.php' ?>
      <?php if (isset($_GET['createFlag2']) && $_GET['createFlag2'] == 1): ?>
      <script type="text/javascript">
      document.getElementById("dropdown-button").click();
      document.getElementById("btn1").click();
    </script>
    <?php endif ?>
    </nav>
    <?php }?>

</div>    
</div>
</nav>

    <div class="jumbotron">
        <!--
  <div class="container text-center">
    <h1>Online Store</h1>      
    <p>Mission, Vission & Values</p>
  </div>
-->
<div class="slideshow-container">

<div class="mySlides fade">
  <div class="numbertext">1 / 3</div>
  <img src="img/Cheetos.jpg" style="width:100%">
 
</div>

<div class="mySlides fade">
  <div class="numbertext">2 / 3</div>
  <img src="img/oreo.jpg" style="width:100%">
</div>

<div class="mySlides fade">
  <div class="numbertext">3 / 3</div>
  <img src="img/sandwich.jpg" style="width:100%">
</div>

</div>
<br>

<div style="text-align:center">
  <span class="dot"></span> 
  <span class="dot"></span> 
  <span class="dot"></span> 
</div>
<script src="javascript/slideshow.js"></script> 
</div>
<!--items in home cart-->
<div class="container" id="foodContent">    
  <div class="row">
    <div class="col-sm-4">
      <div class="panel panel-primary">
        <div class="panel-heading">CHEESE CAKE</div>
        <div class="panel-body"><img src="img/cake.jpeg" class="img-responsive" style="width:100%" alt="Image"></div>
        <form action="index.php?notify=1" method="post">
        <input id="add" type="submit" class="btn btn-danger"value="Add to Cart"/> 
        <!--<input id="num" name="numCake" value=1></input>-->
        <select name="numCake" id="num">
        <?php include 'files/select.php' ?>
        </select>
        <input type="hidden" name="cheesecakeflag" value=1>
        </form>
        <div class="panel-footer">Cheese cake 250/=</div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-danger">
        <div class="panel-heading">OREO</div>
        <div class="panel-body"><img src="img/oreo2.jpeg" class="img-responsive" style="width:100%" alt="Image"></div>
        <form action="index.php?notify=1" method="post">
        <input id="add" type="submit" class="btn btn-danger"value="Add to Cart"/> 
        <!--<input id="num" name="numOreo" value=1></input>-->
        <select name="numOreo" id="num">
        <?php include 'files/select.php' ?>
        </select>
        <input type="hidden" name="oreoflag" value=1>
        </form>
        <div class="panel-footer">Sachet of oreo 50/=</div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-success">
        <div class="panel-heading">TEA</div>
        <div class="panel-body"><img src="img/tea.jpeg" class="img-responsive" style="width:100%" alt="Image"></div>
         <form action="index.php?notify=1" method="post">
        <input id="add" type="submit" class="btn btn-danger"value="Add to Cart"/> 
        <!--<input id="num" name="numTea" value=1></input>-->
        <select name="numTea" id="num">
        <?php include 'files/select.php' ?>
        </select>
        <input type="hidden" name="teaflag" value=1>
        </form>
        <div class="panel-footer">Cup of tea 50/=</div>
      </div>
    </div>
  </div>
</div><br>

  <div class="container">    
  <div class="row">
    <div class="col-sm-4">
      <div class="panel panel-success">
        <div class="panel-heading">MILK</div>
        <div class="panel-body"><img src="img/milk.jpeg" class="img-responsive" style="width:100%" alt="Image"></div>
         <form action="index.php?notify=1" method="post">
        <input id="add" type="submit" class="btn btn-danger"value="Add to Cart"/> 
        <!--<input id="num" name="numMilk" value=1></input>-->
        <select name="numMilk" id="num">
        <?php include 'files/select.php' ?>
        </select>
        <input type="hidden" name="milkflag" value=1>
        </form>
        <div class="panel-footer">Glass of milk 100/=</div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-primary">
        <div class="panel-heading">MAANDAZI</div>
        <div class="panel-body"><img src="img/mand2.jpeg" class="img-responsive" style="width:100%" alt="Image"></div>
        <form action="index.php?notify=1" method="post">
        <input id="add" type="submit" class="btn btn-danger"value="Add to Cart"/> 
        <!--<input id="num" name="numMand" value=1></input>-->
        <select name="numMand" id="num">
        <?php include 'files/select.php' ?>
        </select>
        <input type="hidden" name="mandflag" value=1>
        </form>
        <div class="panel-footer">Maandazi 50/=</div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-danger">
        <div class="panel-heading">MANJI DIGESTIVE BISCUIT</div>
        <div class="panel-body"><img src="img/manji.jpeg" class="img-responsive" style="width:100%" alt="Image"></div>
        <form action="index.php?notify=1" method="post">
        <input id="add" type="submit" class="btn btn-danger"value="Add to Cart"/> 
        <!--<input id="num" name="numManji" value=1></input>-->
        <select name="numManji" id="num">
        <?php include 'files/select.php' ?>
        </select>
        <input type="hidden" name="manjiflag" value=1>
        </form>
        <div class="panel-footer">Manji digestive biscuits 200/=</div>
      </div>
    </div>
  </div>
</div><br><br>
<!--Snack bar handler when you add items to cart-->
<?php if (isset($_GET['notify']) && $_GET['notify'] == 1): ?>
<div id="snackbar">You have added an item to the cart</div>
<script>
window.onload = function() {
    var tag = document.getElementById('foodContent');
    tag.scrollIntoView(true);
    function notify() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);}
    notify();
}
</script>

<?php endif ?>

<!--php to handle click events when you remove an item from cart-->
<?php include 'cartHandler.php'?>
<?php include 'modal.php' ?>
<?php if (isset($_GET['remNotify']) && $_GET['remNotify'] == 1): ?>
  <script type="text/javascript">
    document.getElementById("cartbtn").click();
  </script>
<?php endif ?>
<!--Snackbar after ordering an item-->
<?php if (isset($_GET['orderSnackbar']) && $_GET['orderSnackbar'] == 1): ?>
  <div id="snackbarRed">Your food order has been placed</div>
  <script type="text/javascript">
    function notify() {
    var x = document.getElementById("snackbarRed");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);}
    notify();

  </script>
  
<?php endif ?>






<footer class="container-fluid text-center">
  <p>SGR Online Store Copyright</p>  
  <form class="form-inline">Get deals:
    <input type="email" class="form-control" size="50" placeholder="Email Address">
    <button type="button" class="btn btn-danger">Sign Up</button>
  </form>
</footer>    
</body>
</html>